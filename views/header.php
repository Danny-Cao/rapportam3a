<!doctype html>
<html>
<head>
    <title><?=(isset($this->title)) ? $this->title : 'MVC'; ?></title>
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/default.css" />    
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/sunny/jquery-ui.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>public/js/custom.js"></script>
    <?php 
    if (isset($this->js)) 
    {
        foreach ($this->js as $js)
        {
            echo '<script type="text/javascript" src="'.URL.'views/'.$js.'"></script>';
        }
    }
    ?>
</head>
<body>

<?php Session::init(); ?>
    
<div id="header">
<div id="logobox">
    <a href="http://localhost/mvc/" class="logo">Homepage</a>
</div>
    <?php if (Session::get('loggedIn') == false):?>
        <a href="<?php echo URL; ?>index">Home</a>
        <a href="<?php echo URL; ?>overOns">Over ons</a>
        <a href="<?php echo URL; ?>roosters">Roosters</a>
        <a href="<?php echo URL; ?>contact">Contact</a>
    <?php endif; ?>    
    <?php if (Session::get('loggedIn') == true):?>
        <a href="<?php echo URL; ?>dashboard">Dashboard</a>
       
        
        <?php if (Session::get('role') == 'owner'):?>
        <a href="<?php echo URL; ?>user">Users</a>
        <?php endif; ?>
        
        <?php if (Session::get('role') == 'student'):?>
        <a href="<?php echo URL; ?>student">Student</a>
        <?php endif; ?>
        
        <?php if (Session::get('role') == 'docent'):?>
        <a href="<?php echo URL; ?>docent">Docent</a>
        <?php endif; ?>
        
        <?php if (Session::get('role') == 'roostermaker'):?>
        <a href="<?php echo URL; ?>roostermaker">roostermaker</a>
        <?php endif; ?>
        
        <?php if (Session::get('role') == 'coördinator'):?>
        <a href="<?php echo URL; ?>coordinator">coordinator</a>
        <?php endif; ?>
        
        <a href="<?php echo URL; ?>dashboard/logout">Logout</a>    
    <?php else: ?>
        <a href="<?php echo URL; ?>login">Login</a>
    <?php endif; ?>
</div>
    
<div id="content">
    
    