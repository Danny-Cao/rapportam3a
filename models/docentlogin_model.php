<?php

class DocentLogin_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

       public function docentLogin (){

                $sth = $this->db->prepare("SELECT docentafkorting FROM docent WHERE 
                docentafkorting = :docentafkorting AND wachtwoord = :wachtwoord");
        $sth->execute(array(
            ':docentafkorting' => $_POST['docentafkorting'],
            ':wachtwoord' =>  $_POST['wachtwoord']
        ));
        
        $data = $sth->fetch();
        
        $count =  $sth->rowCount();
        if ($count > 0) {
            // login
            Session::init();
            Session::set('role', 'docent');
            Session::set('loggedIn', true);
            Session::set('docentafkorting', $data['docentafkorting']);
            header('location: ../dashboard');
        } else {
            header('location: ../docentLogin');
        }
    }

    
}