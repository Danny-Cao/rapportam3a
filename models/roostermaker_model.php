<?php

class Roostermaker_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function roosterklaarzet()
    {
        return $this->db->select('SELECT idRooster,klas, versie, leerjaar ,status FROM rooster');
    }

     public function roosterSingleList($idRooster)
    {
        return $this->db->select('SELECT idRooster, klas, versie ,leerjaar ,status FROM rooster WHERE idRooster = :idRooster', array(':idRooster' => $idRooster));
    }
    

    public function editSave($data)
    {
        $postData = array(
            'idRooster' => $data['idRooster'],
            'klas' => $data['klas'],
            'versie' => $data['versie'], 
            'leerjaar' => $data['leerjaar'],
            'status' => $data['status']
        );
        
        $this->db->update('rooster', $postData, "`idRooster` = {$data['idRooster']}");
    }
    
    public function delete($idRooster)
    {
        $result = $this->db->select('SELECT idRooster FROM rooster WHERE idRooster = :idRooster', array(':idRooster' => $idRooster));

        
        $this->db->delete('rooster', "idRooster = '$idRooster'");
    }

    
}