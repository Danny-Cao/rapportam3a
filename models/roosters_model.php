<?php

class Roosters_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function rooster()
    {
        
      return $this->db->select('SELECT idRooster,klas,periode ,status FROM Rooster where status="actief"');
    }
    
    public function vervolg($rooster)
    {
        
      return $this->db->select('SELECT * FROM Les where rooster = :idRooster', array(':idRooster' => $rooster));

    }
 
    
}