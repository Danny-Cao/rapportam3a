<?php
class Docent_Model extends Model{

public function __construct()
{
	parent::__construct();
	
}


	
	public function voegCijferToe($data)
{
	$this->db->insert('cijfer', array(
	'vak' => $data['vak'],
	'docent' => $data['docent'],
	'rapport' => $data['rapport'],
	'cijfer' => $data['cijfer'],
	'weging' => $data['weging']
	));
}

	public function roosterActief(){
		 return $this->db->select('SELECT idRooster,klas,versie ,periode ,leerjaar FROM rooster where status ="actief"');
    }

    public function studentList($klas){
    	 return $this->db->select('SELECT idStudent,klas,voornaam ,tussenvoegsel,achternaam FROM student WHERE klas = :klas', array(':klas' => $klas));

    }

    public function cijferList($student){
    	 return $this->db->select('SELECT idRapport,student,periode,leerjaar FROM rapport WHERE student = :idStudent', array(':idStudent' => $student));
    }

   




	}
?>