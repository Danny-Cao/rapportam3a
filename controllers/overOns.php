<?php

class OverOns extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    function index() {
        
        $this->view->title = 'Over ons';
        $this->view->render('header');
        $this->view->render('overOns/index');
        $this->view->render('footer');
    }
    
}