<?php

class Docent extends Controller {

    function __construct() {
        parent::__construct();
        Auth::handleLogin();
    }
    
    function index() {

        $this->view->title = 'Home';
        $this->view->roosterActief = $this->model->roosterActief();
        $this->view->render('header');
        $this->view->render('docent/index');
        $this->view->render('footer');
    }
    
    
    
     public function voegCijferToe() 
    {
        $data = array();
        $data['vak'] = $_POST['vak'];
        $data['docent'] = $_POST['docent'];
        $data['rapport'] = $_POST['rapport'];
        $data['cijfer'] = $_POST['cijfer'];
        $data['weging'] = $_POST['weging'];
        
        $this->model->voegCijferToe($data);
        header('location: ' . URL . 'docent');
    }

      public function studentList($klas) 
    {
        $this->view->title = 'studentList';
        $this->view->studentList = $this->model->studentList($klas);
        $this->view->render('header');
        $this->view->render('docent/studentList');
        $this->view->render('footer');
    }
    
     public function cijferList($student) 
    {
        $this->view->title = 'studentList';
        $this->view->cijferList = $this->model->cijferList($student);
        $this->view->render('header');
        $this->view->render('docent/cijferList');
        $this->view->render('footer');
    }
    
 
    
}