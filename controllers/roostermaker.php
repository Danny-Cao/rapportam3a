<?php

class Roostermaker extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    function index() {

        $this->view->title = 'Home';
        $this->view->roosterklaarzet = $this->model->roosterklaarzet();
        $this->view->render('header');
        $this->view->render('roostermaker/index');
        $this->view->render('footer');
    }

     public function edit($idRooster) 
    {
        $this->view->title = 'Edit Rooster';
        $this->view->rooster = $this->model->roosterSingleList($idRooster);
        $this->view->render('header');
        $this->view->render('roostermaker/edit');
        $this->view->render('footer');
    }

    public function editSave($idRooster)
    {
        $data = array();
        $data['idRooster'] = $idRooster;
        $data['klas'] = $_POST['klas'];
        $data['versie'] = $_POST['versie'];
        $data['leerjaar'] = $_POST['leerjaar'];
        $data['status'] = $_POST['status'];
        
        // @TODO: Do your error checking!
        
        $this->model->editSave($data);
        header('location: ' . URL . 'roostermaker');
    }
    
}