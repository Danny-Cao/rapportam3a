<?php

class DocentLogin extends Controller {

    function __construct() {
        parent::__construct();    
    }
    
    function index() 
    {    
        $this->view->title = 'Docent Login';
        
        $this->view->render('header');
        $this->view->render('docentlogin/index');
        $this->view->render('footer');
    }
    
    function docentlogin()
    {
        $this->model->docentlogin();
    }

    
    

}