<?php

class Roosters extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    function index() {

        $this->view->title = 'Roosters';
        $this->view->rooster = $this->model->rooster();

        $this->view->render('header');
        $this->view->render('roosters/index');
        $this->view->render('footer');
    }


    function vervolg($rooster){

        $this->view->title = 'Roosters';
        $this->view->vervolg = $this->model->vervolg($rooster); 
        $this->view->render('header');
        $this->view->render('roosters/vervolg');
        $this->view->render('footer');
    }
    
     
    
}